<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyShops extends Command
{
    public $db_host = 'mariadb';
    public $db_user = 'root';
    public $db_pass = 'root';
    public $db_database = 'stocks';

    private function getConnection()
    {
        return new \PDO("mysql:host={$this->db_host};dbname={$this->db_database}", $this->db_user, $this->db_pass);
    }

    protected static $defaultName = 'notify:shops';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $connection = $this->getConnection();
        $result = $connection->query("SELECT * FROM stocks")->fetchAll();

        foreach ($result as $stock) {
            $this->notifyShop1($stock);
            $this->notifyShop2($stock);
            $this->notifyShop3($stock);
            $this->notifyShop4($stock);
        }

        $output->writeln('Sent successfully');

        return Command::SUCCESS;
    }

    protected function notifyShop1($stock) {
        exec(
            "curl -X POST -F 'item={$stock['id']}' -F 'stock={$stock['stock']}' https://ksi.nfq.lt/stock.php"
        );
    }

    protected function notifyShop2($stock) {
        exec(
            "curl -X POST -d 'item={$stock['id']}' -d 'stock={$stock['stock']}' https://shop.nfq.lt/stock.php"
        );
    }

    protected function notifyShop3($stock) {
        exec(
            "curl -X POST -H 'Content-Type: application/json' -d '{\"item\": \"{$stock['id']}\", \"stock\": \"{$stock['stock']}\"}' https://shop2.nfq.lt/stock.php"
        );
    }

    protected function notifyShop4($stock) {
        exec(
            "curl -X POST -H 'Content-Type: application/json' -d '{\"item\": \"{$stock['id']}\", \"stock\": \"{$stock['stock']}\"}' https://shop3.nfq.lt/stock.php"
        );
    }
}