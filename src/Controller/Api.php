<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\JsonResponse;

class Api
{
    public $db_host = 'mariadb';
    public $db_user = 'root';
    public $db_pass = 'root';
    public $db_database = 'stocks';

    private function getConnection()
    {
        return new \PDO("mysql:host={$this->db_host};dbname={$this->db_database}", $this->db_user, $this->db_pass);
    }

    public function getStocks($id)
    {
        $connection = $this->getConnection();

        $result = $connection->query("SELECT * FROM stocks WHERE id = {$id}")->fetchAll();

        return new JsonResponse($result);

    }

    public function postNewStock()
    {
        $id = $_POST['id'];
        $stock = $_POST['stock'];

        $connection = $this->getConnection();

        $sql = "INSERT INTO stocks (id, stock, reserved) VALUES (?,?)";
        $stmt= $connection->prepare($sql);
        $stmt->execute([$id, $stock, 0]);

        $result = $connection->query("SELECT * FROM stocks WHERE id = {$id}")->fetchAll();

        return new JsonResponse($result);
    }

    public function makeReservation($id)
    {
        $amount = $_POST['amount'];
        $connection = $this->getConnection();

        $sql = "UPDATE stocks SET reserved = reserved+{$amount} WHERE id = {$id}";
        $stmt= $connection->prepare($sql);
        $stmt->execute();

        $result = $connection->query("SELECT * FROM stocks WHERE id = {$id}")->fetchAll();

        return new JsonResponse($result);

    }
}