<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class Admin
{
    public $db_host = 'mariadb';
    public $db_user = 'root';
    public $db_pass = 'root';
    public $db_database = 'stocks';

    private function getConnection()
    {
        return new \PDO("mysql:host={$this->db_host};dbname={$this->db_database}", $this->db_user, $this->db_pass);
    }

    public static $password = 'superSecretPassword';

    public function showAllStocksAction()
    {
        $connection = $this->getConnection();

        if ($_GET['pass'] == self::$password) {
            $result = $connection->query("SELECT * FROM stocks")->fetchAll();
            $table = "<table style='border-collapse: collapse; border: 1px solid'>";
            $table .= "
                <tr style='border-collapse: collapse; border: 1px solid'>
                    <td style='border-collapse: collapse; border: 1px solid'>ID</td>
                    <td style='border-collapse: collapse; border: 1px solid'>Stock</td>
                    <td style='border-collapse: collapse; border: 1px solid'>Reserved</td>
                </tr>
                ";
            foreach ($result as $row) {
                $table .= "
                <tr style='border-collapse: collapse; border: 1px solid'>
                    <td style='border-collapse: collapse; border: 1px solid'>{$row['id']}</td>
                    <td style='border-collapse: collapse; border: 1px solid'>{$row['stock']}</td>
                    <td style='border-collapse: collapse; border: 1px solid'>{$row['reserved']}</td>
                </tr>
                ";
            }

            return new Response($table);
        }

        return new RedirectResponse('/admin/login');
    }

    public function showLoginForm()
    {
        $form = file_get_contents(__DIR__ . '/login.html');

        return new Response($form);
    }
}