# Background
We have an old application which we call stock service. Basically it's the application which is source of truth of all eshop inventory stocks.

It has an API methods to make reservation (reserve stock), get stock and post new inventory stock.

There is also a basic admin zone, where admin user can log in and check all the stocks.

The last functionality is command which is triggered to send up to date stocks to all shops.

# Task
You have to organise refactoring of this in steps, then implement it. 

* First commit should contain refactoring plan.
* Every commit in the flow should represent what have been done.
* You can use libraries, just keep in mind, don't add too much of them.
* I'm not sure if docker-compose file really works, it might need some improvements. :)
* We should use as much Symfony standard functionalities as possible.
* Structure of API requests can't be changed. However, you are expected to change code structure, clean it up, refactor to clean code.

# Instructions
## Starting the project
```
docker-compose up
```

Accessible at: http://127.0.0.1:8000/

## Importing database dump
```
docker exec -i ksi-mysql mysql -u root -proot -h 127.0.0.1 < database.sql
```

## Accessible routes

* GET /admin/login
* GET /admin/list
* GET /api/get/{id}
* POST /api/post/{id}
* POST /api/reserve/{id}

# Requirements

* Use object-oriented programming
* Use design patterns where possible
* Use tests where possible
* Use Boy Scout rule

# Helpers

* https://www.youtube.com/playlist?list=PL-6xmoyU9moG3bD3cbTrAU4IEmgKWlS3l
* https://sourcemaking.com/